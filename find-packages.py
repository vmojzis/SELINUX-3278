#!/usr/bin/python3

import dnf
import json
import subprocess
import sys

semodule = subprocess.run(['semodule', '-lfull'], capture_output=True)

if len(sys.argv) == 1:
    modules = [x.split() for x in str(semodule.stdout, "utf-8").split('\n')]
else:
    modules = [sys.argv[1].split()]

base = dnf.Base()
conf = base.conf
conf.substitutions['stream'] = '9-stream'

base.read_all_repos()
base.fill_sack(load_system_repo=True)

module_packages = list()

for module in modules:
    f = None
    repo_id = "none"
    repo_name = "none"

    if module == []:
        continue
    if module[0] != '100':
        continue
    module_name = module[1]
    for d in ["admin", "apps", "contrib", "kernel", "roles", "services", "system" ]:
        try:
            f = open("selinux-policy/policy/modules/{}/{}.fc".format(d, module_name), "r")
            break
        except:
            continue

    if f is None:
        module_packages.append({
            "module_name": module_name,
            "repo_id": None
        })
        print("module {} not found".format(module_name), file=sys.stderr)
        continue

    fcs = []
    fcs_f = []
    for x in f:
        try:
            fc = x.split()[0]
            fcs.append(fc)
        except:
            continue

        # print("fcontext: ", fc)

        # "/root/\\.pki(/.*)?",
        fc = fc.replace('\\', '')
        fc = fc.replace('(/.*)?', '')

        if fc.endswith('.*'):
            print("file__substr={}".format(fc), file=sys.stderr)
            fc = fc.replace('.*', '')
            q = base.sack.query().filter(file__substr=fc)
        else:
            q = base.sack.query().filter(file=fc)

        fcs_f.append(fc)

        if q.count() == 0:
            continue
        for p in q:
            try:
                repo_id = p.repo.id
                if repo_id != "epel":
                    break
            except:
                pass
            try:
                repo_name = p.repo.name
            except:
                pass
            # print(p.repo.name)

        if repo_id != "none" and repo_id != "epel":
          break

    module_package = {
        "module_name": module_name,
        "repo_id": repo_id,
        "fcs": fcs,
        "fcs_f": fcs_f
    }
    module_packages.append(module_package)

    if repo_id == "none":
    #     print(fcs, file=sys.stderr)
        print(json.dumps(module_package, indent=2), flush=True, file=sys.stderr)
    else:
        print(json.dumps({ "module_name": module_name, "repo_id": repo_id }, indent=2), flush=True, file=sys.stderr)

    repo_id = "none"
    repo_name = "none"

print(json.dumps(module_packages, indent=2))
